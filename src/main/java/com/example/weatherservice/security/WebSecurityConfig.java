package com.example.weatherservice.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final String ADMIN_ROLE = "ADMIN";
    private final String USER_ROLE = "USER";

    @Value("${application.user.password:user}")
    private String userPassword;

    @Value("${application.admin.password:admin}")
    private String adminPassword;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/weather").authenticated()
                .antMatchers(HttpMethod.GET, "/actuator/**").hasRole(ADMIN_ROLE)
                .antMatchers("/login").permitAll()
                .anyRequest().permitAll()
                .and()
                .formLogin().successHandler((req, res, auth) -> res.setStatus(HttpStatus.NO_CONTENT.value()))
                .and()
                .logout();
    }

    // It's just a simple authorization which should be replaced in next version of the application. OAuth2 should be implemented
    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        UserDetails user =
                User.withDefaultPasswordEncoder()
                        .username("user")
                        .password(userPassword)
                        .roles(USER_ROLE)
                        .build();
        UserDetails admin =
                User.withDefaultPasswordEncoder()
                        .username("admin")
                        .password(adminPassword)
                        .roles(ADMIN_ROLE)
                        .build();

        return new InMemoryUserDetailsManager(user, admin);
    }

}
