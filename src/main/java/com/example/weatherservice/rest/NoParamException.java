package com.example.weatherservice.rest;

class NoParamException extends RuntimeException {

    public NoParamException(String s) {
        super(s);
    }
}
