package com.example.weatherservice.rest;

import com.example.weatherservice.client.WeatherClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/weather")
@RestController
public class WeatherController {

    private final WeatherClient weatherClient;

    public WeatherController(WeatherClient weatherClient) {
        this.weatherClient = weatherClient;
    }

    @GetMapping(params = {"postalcode"})
    public ResponseEntity getWeatherForCity(@RequestParam("postalcode") String postalCode) {
        return ResponseEntity.ok(weatherClient.get5DaysForecast(postalCode));
    }
}
