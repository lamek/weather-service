package com.example.weatherservice.client.accuweather;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
class WeatherForecast {
    @JsonProperty("DailyForecasts")
    List<DailyForecast> forecasts;
}
