package com.example.weatherservice.client.accuweather;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
class Location {

    @JsonProperty("Key")
    private String key;
    @JsonProperty("LocalizedName")
    String name;
    @JsonProperty("AdministrativeArea")
    private Area area;
    @JsonProperty("PrimaryPostalCode")
    private String  postalCode;
    @JsonProperty("Country")
    private Country country;
}
