package com.example.weatherservice.client.accuweather;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Data
@Component
class ApiKey {
    private String key;
    private ApiConfigurationProperties apiConfigurationProperties;

    @Autowired
    public ApiKey(ApiConfigurationProperties apiConfigurationProperties) {
        key = apiConfigurationProperties.getApiKey();
    }

    public ApiKey(String apiKey) {
        key = apiKey;
    }
}
