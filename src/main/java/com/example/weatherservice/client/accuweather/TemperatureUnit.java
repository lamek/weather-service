package com.example.weatherservice.client.accuweather;

enum TemperatureUnit {
    F, C
}