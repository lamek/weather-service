package com.example.weatherservice.client.accuweather;

import com.example.weatherservice.client.Forecast;
import com.example.weatherservice.client.Temperature;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
class ForecastMapper {

    Forecast buildForecast(Location location, List<DailyForecast> forecasts) {
        List<Temperature> temperatures = forecasts.stream()
                .map(this::map)
                .collect(Collectors.toList());

        return Forecast.builder()
                .area(location.getArea().getName())
                .postalCode(location.getPostalCode())
                .country(location.getCountry().getName())
                .temperatures(temperatures)
                .name(location.getName())
                .build();
    }

    private Temperature map(DailyForecast forecast) {
        return new Temperature(
                forecast.getDate(),
                forecast.getTemperature().maximumAsString(),
                forecast.getTemperature().minimumAsString()
        );
    }
}
