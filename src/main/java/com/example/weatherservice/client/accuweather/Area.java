package com.example.weatherservice.client.accuweather;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Area {
    @JsonProperty("ID")
    private String id;
    @JsonProperty("LocalizedName")
    private String name;
}
