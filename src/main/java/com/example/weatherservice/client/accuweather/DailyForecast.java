package com.example.weatherservice.client.accuweather;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
class DailyForecast {
    @JsonProperty("Date")
    private Date date;
    @JsonProperty("Temperature")
    private Temperature temperature;

    @Data
    @NoArgsConstructor
    static class Temperature {
        @JsonProperty("Minimum")
        private TemperatureValue minimum;
        @JsonProperty("Maximum")
        private TemperatureValue maximum;

        String minimumAsString() {
            return String.format("%s %s", minimum.getValue(), minimum.getUnit().toString());
        }

        String maximumAsString() {
            return String.format("%s %s", maximum.getValue(), maximum.getUnit().toString());
        }
    }
}
