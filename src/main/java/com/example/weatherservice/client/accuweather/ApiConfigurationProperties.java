package com.example.weatherservice.client.accuweather;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("weather.client")
@NoArgsConstructor
@Data
class ApiConfigurationProperties {
    private String apiKey;
}
