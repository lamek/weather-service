package com.example.weatherservice.client.accuweather;

import com.example.weatherservice.client.ClientException;
import com.example.weatherservice.client.Forecast;
import com.example.weatherservice.client.WeatherClient;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Component
class AccuweatherClientImpl implements WeatherClient {

    private ApiKey apiKey;

    private final RestTemplate restTemplate = new RestTemplate();

    private final ForecastMapper forecastMapper = new ForecastMapper();

    public AccuweatherClientImpl(ApiKey apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public List<Forecast> get5DaysForecast(String postalCode) {
        List<Location> locations = getLocation(postalCode);
        return locations.stream()
                .map(l -> forecastMapper.buildForecast(l, getTemperatureForLocation(l.getKey())))
                .collect(Collectors.toList());
    }

    private List<Location> getLocation(String postalCode) {

        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme("http").host("dataservice.accuweather.com")
                .path("/locations/v1/search")
                .queryParam("q", postalCode)
                .queryParam("apikey", apiKey.getKey())
                .build();

        Location[] locations;
        try {
            locations = restTemplate.getForObject(uriComponents.toUriString(), Location[].class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            log.error(e);
            throw new ClientException(e.getMessage());
        }

        return Arrays.asList(locations);
    }

    private List<DailyForecast> getTemperatureForLocation(String locationKey) {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme("http").host("dataservice.accuweather.com")
                .path(String.format("/forecasts/v1/daily/5day/%s", locationKey))
                .queryParam("metric", true)
                .queryParam("apikey", apiKey.getKey())
                .build();

        WeatherForecast response = null;
        try {
            response = restTemplate.getForObject(uriComponents.toUriString(), WeatherForecast.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            log.error(e);
            throw new ClientException(e.getMessage());
        }
        return response.getForecasts();
    }
}
