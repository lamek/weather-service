package com.example.weatherservice.client.accuweather;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
class TemperatureValue {
    @JsonProperty("Value")
    private double value;
    @JsonProperty("Unit")
    private TemperatureUnit unit;
}