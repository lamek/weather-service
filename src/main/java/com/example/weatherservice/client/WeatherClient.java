package com.example.weatherservice.client;

import java.util.List;

public interface WeatherClient {
    List<Forecast> get5DaysForecast(String postalCode);
}
