package com.example.weatherservice.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Forecast {
    private String postalCode;
    private String name;
    private String area;
    private String country;
    private List<Temperature> temperatures;

}
