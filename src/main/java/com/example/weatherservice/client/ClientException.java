package com.example.weatherservice.client;

public class ClientException extends RuntimeException {
    public ClientException(String s) {
        super(s);
    }
}
