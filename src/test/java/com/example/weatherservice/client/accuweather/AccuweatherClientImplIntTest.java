package com.example.weatherservice.client.accuweather;

import com.example.weatherservice.client.ClientException;
import com.example.weatherservice.client.Forecast;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@SpringBootTest
class AccuweatherClientImplIntTest {

    @Autowired
    ApiConfigurationProperties apiConfigurationProperties;

    @Test
    void shouldThrowClientException_whenInvalidApiKeyIsProvided() {
        AccuweatherClientImpl client = new AccuweatherClientImpl(new ApiKey("none"));
        assertThrows(ClientException.class, () ->
                client.get5DaysForecast("42-300"));
    }

    @Test
    void shouldReturnSingletonList_whenValidPostCodeAndApiKeyIsProvided() {
        AccuweatherClientImpl client = new AccuweatherClientImpl(new ApiKey(apiConfigurationProperties.getApiKey()));
        List<Forecast> forecasts = client.get5DaysForecast("42-300");
        assertNotNull(forecasts);
        assertEquals(1, forecasts.size());
        assertEquals("42-300", forecasts.get(0).getPostalCode());
        assertEquals("Myszkow", forecasts.get(0).getName());
        assertEquals(5, forecasts.get(0).getTemperatures().size());
    }

    @Test
    void shouldReturnEmptyList_whenInvalidPostCodeAndApiKeyIsProvided() {
        AccuweatherClientImpl client = new AccuweatherClientImpl(new ApiKey(apiConfigurationProperties.getApiKey()));
        List<Forecast> forecasts = client.get5DaysForecast("noValidPostalCode123");
        assertNotNull(forecasts);
        assertEquals(0, forecasts.size());
    }
}
