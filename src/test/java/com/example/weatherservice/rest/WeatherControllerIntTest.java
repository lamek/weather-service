package com.example.weatherservice.rest;

import com.example.weatherservice.client.Forecast;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("test")
@SpringBootTest
class WeatherControllerIntTest {

    @Autowired
    private WeatherController weatherController;

    private MockMvc mockMvc;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @PostConstruct
    private void setMockMvc() {
        mockMvc = MockMvcBuilders.standaloneSetup(weatherController).build();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void shouldReturnBadRequest_whenNoPostalCodeIsProvided() throws Exception {
        mockMvc.perform(get("/weather"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void shouldReturnOkWithForecast_whenValidPostalCodeIsProvided() throws Exception {
        MvcResult result = mockMvc.perform(get("/weather").param("postalcode","42-300"))
                .andExpect(status().isOk())
                .andReturn();

        Forecast[] forecasts = objectMapper.readValue(result.getResponse().getContentAsString(), Forecast[].class);
        assertNotNull(forecasts);
        assertEquals(1, forecasts.length);
        assertEquals("42-300", forecasts[0].getPostalCode());
        assertEquals("Myszkow", forecasts[0].getName());
        assertEquals(5, forecasts[0].getTemperatures().size());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void shouldReturnOkWithEmptyList_whenInvalidPostalCodeIsProvided() throws Exception {
        MvcResult result = mockMvc.perform(get("/weather").param("postalcode","NoValidPostalCode"))
                .andExpect(status().isOk())
                .andReturn();

        Forecast[] forecasts = objectMapper.readValue(result.getResponse().getContentAsString(), Forecast[].class);
        assertNotNull(forecasts);
        assertEquals(0, forecasts.length);
    }
}
