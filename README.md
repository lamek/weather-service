## Simple Example of Integration with AccuWeather

### Requirements
JDK11, Maven, AccuWeather API KEY

### Building and Running Application Note
You can skip passing additional arguments on building and running if you adjust application.properties and  application-test.properties files

### Building application
mvn clean package -Dweather.client.apiKey=*$YOUR_TEST_API_KEY*

### Running application
java -jar weather-service-0.0.1-SNAPSHOT.jar --weather.client.apiKey=*$YOUR_API_KEY*

### Running application with customized user and admin passwords
java -jar weather-service-0.0.1-SNAPSHOT.jar --weather.client.apiKey=*$YOUR_API_KEY* --application.admin.password=*$ADMIN_PASS* --application.user.password=*$USER_PASS*

### Endpoints
Login to app
> /login?username=admin&password=admin

Retrieve weather forecast for postal code (can be accessed by any user)
> /weather?postalcode=1112

Get statistics from spring actuator Example (can be accessed only by admin account)
>/actuator/metrics/http.server.requests?tag=uri:/weather